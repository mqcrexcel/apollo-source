#include <EnableInterrupt.h>

#define SERIAL_PORT_SPEED 115200
#define RC_NUM_CHANNELS  4

#define RC_CH1  0
#define RC_CH2  1
#define RC_CH3  2
#define RC_CH4  3

#define RC_CH1_INPUT  A0
#define RC_CH2_INPUT  A1
#define RC_CH3_INPUT  A2
#define RC_CH4_INPUT  A3

#define M1_DIR 2  
#define M1_PWM 3
#define M2_DIR 4  
#define M2_PWM 5

#define BUZZER 12

#define DOWN1 1380
#define UP1 1520
#define MIN1 1080
#define MAX1 1880

#define DOWN2 1420
#define UP2 1580 
#define MIN2 1100
#define MAX2 1900
 
#define NOTE_B0  31
#define NOTE_C1  33
#define NOTE_CS1 35
#define NOTE_D1  37
#define NOTE_DS1 39
#define NOTE_E1  41
#define NOTE_F1  44
#define NOTE_FS1 46
#define NOTE_G1  49
#define NOTE_GS1 52
#define NOTE_A1  55
#define NOTE_AS1 58
#define NOTE_B1  62
#define NOTE_C2  65
#define NOTE_CS2 69
#define NOTE_D2  73
#define NOTE_DS2 78
#define NOTE_E2  82
#define NOTE_F2  87
#define NOTE_FS2 93
#define NOTE_G2  98
#define NOTE_GS2 104
#define NOTE_A2  110
#define NOTE_AS2 117
#define NOTE_B2  123
#define NOTE_C3  131
#define NOTE_CS3 139
#define NOTE_D3  147
#define NOTE_DS3 156
#define NOTE_E3  165
#define NOTE_F3  175
#define NOTE_FS3 185
#define NOTE_G3  196
#define NOTE_GS3 208
#define NOTE_A3  220
#define NOTE_AS3 233
#define NOTE_B3  247
#define NOTE_C4  262
#define NOTE_CS4 277
#define NOTE_D4  294
#define NOTE_DS4 311
#define NOTE_E4  330
#define NOTE_F4  349
#define NOTE_FS4 370
#define NOTE_G4  392
#define NOTE_GS4 415
#define NOTE_A4  440
#define NOTE_AS4 466
#define NOTE_B4  494
#define NOTE_C5  523
#define NOTE_CS5 554
#define NOTE_D5  587
#define NOTE_DS5 622
#define NOTE_E5  659
#define NOTE_F5  698
#define NOTE_FS5 740
#define NOTE_G5  784
#define NOTE_GS5 831
#define NOTE_A5  880
#define NOTE_AS5 932
#define NOTE_B5  988
#define NOTE_C6  1047
#define NOTE_CS6 1109
#define NOTE_D6  1175
#define NOTE_DS6 1245
#define NOTE_E6  1319
#define NOTE_F6  1397
#define NOTE_FS6 1480
#define NOTE_G6  1568
#define NOTE_GS6 1661
#define NOTE_A6  1760
#define NOTE_AS6 1865
#define NOTE_B6  1976
#define NOTE_C7  2093
#define NOTE_CS7 2217
#define NOTE_D7  2349
#define NOTE_DS7 2489
#define NOTE_E7  2637
#define NOTE_F7  2794
#define NOTE_FS7 2960
#define NOTE_G7  3136
#define NOTE_GS7 3322
#define NOTE_A7  3520
#define NOTE_AS7 3729
#define NOTE_B7  3951
#define NOTE_C8  4186
#define NOTE_CS8 4435
#define NOTE_D8  4699
#define NOTE_DS8 4978

#define melodyPin 12

//Mario main theme melody
int melody[] = {
  NOTE_E7, NOTE_E7, 0, NOTE_E7,
  0, NOTE_C7, NOTE_E7, 0,
  NOTE_G7, 0, 0,  0
};

int tempo[] = {
  12, 12, 12, 12,
  12, 12, 12, 12,
  12, 12, 12, 12,
  12, 12, 12, 12,
 
  12, 12, 12, 12,
  12, 12, 12, 12,
  12, 12, 12, 12,
  12, 12, 12, 12,
 
  9, 9, 9,
  12, 12, 12, 12,
  12, 12, 12, 12,
  12, 12, 12, 12,
 
  12, 12, 12, 12,
  12, 12, 12, 12,
  12, 12, 12, 12,
  12, 12, 12, 12,
 
  9, 9, 9,
  12, 12, 12, 12,
  12, 12, 12, 12,
  12, 12, 12, 12,
};
uint16_t rc_values[RC_NUM_CHANNELS];
uint32_t rc_start[RC_NUM_CHANNELS];
volatile uint16_t rc_shared[RC_NUM_CHANNELS];


int pwm1, pwm2, dir1, dir2;


void sing() {
  // iterate over the notes of the melody:
 
 
    Serial.println(" 'Mario Theme'");
    int size = sizeof(melody) / sizeof(int);
    for (int thisNote = 0; thisNote < size; thisNote++) {
 
      // to calculate the note duration, take one second
      // divided by the note type.
      //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
      int noteDuration = 1000 / tempo[thisNote];
 
      buzz(melodyPin, melody[thisNote], noteDuration);
 
      // to distinguish the notes, set a minimum time between them.
      // the note's duration + 30% seems to work well:
      int pauseBetweenNotes = noteDuration * 1.30;
      delay(pauseBetweenNotes);
 
      // stop the tone playing:
      buzz(melodyPin, 0, noteDuration);
 
    }
  
}
 
void buzz(int targetPin, long frequency, long length) {
  //digitalWrite(13, HIGH);
  long delayValue = 1000000 / frequency / 2; // calculate the delay value between transitions
  //// 1 second's worth of microseconds, divided by the frequency, then split in half since
  //// there are two phases to each cycle
  long numCycles = frequency * length / 1000; // calculate the number of cycles for proper timing
  //// multiply frequency, which is really cycles per second, by the number of seconds to
  //// get the total number of cycles to produce
  for (long i = 0; i < numCycles; i++) { // for the calculated length of time...
    digitalWrite(targetPin, LOW); // write the buzzer pin high to push out the diaphram
    delayMicroseconds(delayValue); // wait for the calculated delay value
    digitalWrite(targetPin, HIGH); // write the buzzer pin low to pull back the diaphram
    delayMicroseconds(delayValue); // wait again or the calculated delay value
  }
  //digitalWrite(13, LOW);
 
}


void motor1_Move(int dir, int pwm)
{
    digitalWrite(M1_DIR, dir);
    analogWrite(M1_PWM, pwm);
  }

void motor2_Move(int dir, int pwm)
{
    digitalWrite(M2_DIR, dir);
    analogWrite(M2_PWM, pwm);
  }

void rc_read_values() {
  noInterrupts();
  memcpy(rc_values, (const void *)rc_shared, sizeof(rc_shared));
  interrupts();
}

void calc_input(uint8_t channel, uint8_t input_pin) {
  if (digitalRead(input_pin) == HIGH) {
    rc_start[channel] = micros();
  } else {
    uint16_t rc_compare = (uint16_t)(micros() - rc_start[channel]);
    rc_shared[channel] = rc_compare;
  }
}

void calc_ch1() { calc_input(RC_CH1, RC_CH1_INPUT); }
void calc_ch2() { calc_input(RC_CH2, RC_CH2_INPUT); }
void calc_ch3() { calc_input(RC_CH3, RC_CH3_INPUT); }
void calc_ch4() { calc_input(RC_CH4, RC_CH4_INPUT); }

void setup() {
  Serial.begin(SERIAL_PORT_SPEED);

  pinMode(RC_CH1_INPUT, INPUT);
  pinMode(RC_CH2_INPUT, INPUT);
  pinMode(RC_CH3_INPUT, INPUT);
  pinMode(RC_CH4_INPUT, INPUT);
  
  pinMode(M1_DIR, OUTPUT);
  pinMode(M1_PWM, OUTPUT);
  pinMode(M2_DIR, OUTPUT);
  pinMode(M2_PWM, OUTPUT);

  pinMode(BUZZER, OUTPUT);

  enableInterrupt(RC_CH1_INPUT, calc_ch1, CHANGE);
  enableInterrupt(RC_CH2_INPUT, calc_ch2, CHANGE);
  enableInterrupt(RC_CH3_INPUT, calc_ch3, CHANGE);
  enableInterrupt(RC_CH4_INPUT, calc_ch4, CHANGE);

  digitalWrite(M1_DIR, 0);
  digitalWrite(M2_DIR, 0); 
  analogWrite(M1_PWM, 0);
  analogWrite(M2_PWM, 0); 

  sing();  
}

void loop() {
  rc_read_values();

  Serial.print("CH1:"); Serial.print(rc_values[RC_CH1]); Serial.print("\t");
  Serial.print("PWM1:"); Serial.print(pwm1); Serial.print("\t");
  Serial.print("CH2:"); Serial.print(rc_values[RC_CH2]); Serial.print("\t");
  Serial.print("PWM2:"); Serial.println(pwm2);
  //Serial.print("PWM1:"); Serial.println(pwm1);
    
    if(rc_values[RC_CH1] > DOWN1 && rc_values[RC_CH1] < UP1 && rc_values[RC_CH2] > DOWN2 && rc_values[RC_CH2] < UP2) // DUNG OK
  {
    pwm1 = pwm2 = dir1 = dir2 = 0; // Dung Robot
  }
    
    if(rc_values[RC_CH1] > UP1 && rc_values[RC_CH2] > DOWN2 && rc_values[RC_CH2] < UP2) // Joy 1 LUI. Joy2 dung yen
  {
    pwm1 = pwm2 = map(rc_values[RC_CH1],1520,1880,0,255); // Robot LUI
    dir1 = 1;
    dir2 = 0;
  }

    if(rc_values[RC_CH1] < DOWN1 && rc_values[RC_CH2] > DOWN2 && rc_values[RC_CH2] < UP2) // Joy 1 TIEN, Joy2 dung yen
  {
    pwm1 = pwm2 = map(rc_values[RC_CH1],1380,1080,0,255); // Robot TIEN
    dir1 = 0;
    dir2 = 1;
  }

    if(rc_values[RC_CH1] > DOWN1 && rc_values[RC_CH1] < UP1 && rc_values[RC_CH2] > UP2) // Joy 1 TIEN, Joy2 dung yen
  {
    pwm1 = pwm2 = map(rc_values[RC_CH2],1580,1900,0,255); // Robot xoay trai
    dir1 = 1;
    dir2 = 1;
  }

    if(rc_values[RC_CH1] > DOWN1 && rc_values[RC_CH1] < UP1 && rc_values[RC_CH2] < DOWN2) // Joy 1 TIEN, Joy2 dung yen
  {
    pwm1 = pwm2 = map(rc_values[RC_CH2],1420,1100,0,255); // Robot xoay phai
    dir1 = 0;
    dir2 = 0;
  }


    if( (rc_values[RC_CH1] < DOWN1 || rc_values[RC_CH1] > UP1) && rc_values[RC_CH2] < DOWN2) // Sang phai
  {
    pwm2 = 0;
    dir2 = 0;
  }

    if( (rc_values[RC_CH1] < DOWN1 || rc_values[RC_CH1] > UP1) && rc_values[RC_CH2] > UP2) // Sang TRAi
  {
    pwm1 = 0;
    dir1 = 0;
  }


  motor1_Move(dir1, pwm1);
  motor2_Move(dir2, pwm2);


}
